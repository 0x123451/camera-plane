# Camera plane
Import images and parent them to the camera. You can then set the
distance and width from the image object’s or the camera’s properties.
The plane will adjust to the camera’s FOV or focal length.  
You can easily import several images at once, which will be equally
spaced in depth. This is useful when creating painted stage-like sets
which need to stick to a camera.

![UI](docs/camera_plane_ui.png "Camera Planes UI")


## Installation
- Download the [`camera_plane.py`](https://gitlab.com/lfs.coop/blender/camera-plane/-/raw/master/camera_plane.py?inline=false) file;
- In Blender, go to the User Preferences > Add-ons tab, and select Install…
- Select the Python file from where you downloaded it;
- Activate the add-on by clicking the checkbox to the left of it;
- In the filter field to the top left of the window, type in “images”;
- Activate the *Import Images as Planes* add-on which should appear.


## Workflow
### Importing
- Select the camera;
- Click Import Camera Planes from the object properties;
- Select images to import;
- The panel now extends to show a list of images.
- Those images can be filtered, and they can be displayed either in
  alphabetical order, or increasing in distance from the camera.

![Focal 1](docs/camera_plane_focal1.png "Focal 1") ![Focal 2](docs/camera_plane_focal2.png "Focal 2")  
The only thing that was changed between those two images is the
camera’s focal length, and all planes were scaled accordingly to fit
the frame.

### Image settings
From the Camera Plane panel, you can reorder the planes by setting
their distance from the camera. Like other properties in Blender, you
can click on a distance, drag up or down to include the distance from
other planes, and then drag left or right to change the distance, and
thus move several planes at once.

The scale of each image can likewise be tweaked.

If you select multiple planes, you can "bake them" together to create
a single, new image. This may be useful if you have many images that
constitute a single plane, such as a level of overlay.

When the planes are set up, you can click the Setup Layers button to
create as many view layers as there are planes, and assign them to
collections. That way, they can be rendered and composited individually.

Please see [this
article](http://lacuisine.tech/2017/10/21/cameraplane-a-tool-for-2d-sets/)
for a more in-depth overview of the tool. The interface has since
changed, but the principle is the same.


## License

Blender scripts published by **Les Fées Spéciales** are, except where
otherwise noted, licensed under the GPLv2 license.
